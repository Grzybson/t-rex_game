package gameElements.obstacles;

import gameElements.Dinosaur;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class ObstaclesTest {

    Obstacles obstacles = new Obstacles(100);

    @Test
    public void obstacleCollision_dinosaur_injured_should_return_true() {

        Dinosaur dinosaur = new Dinosaur();

        dinosaur.injured();
        boolean result = obstacles.obstacleCollision();

        assertTrue(result);

    }

    @Test
    public void set_random_interval_should_give_value_between_200_and_600() {

        int min = 200;
        int max = 600;

        int result = obstacles.setObstacleInterval();

        boolean value = result >= min && result <= max;

        System.out.println("random numer:" + result + " is between 200 - 600");

        assertTrue(value);

    }

}