package gamePanel;

import gameElements.Dinosaur;
import gameElements.Landscape;
import gameElements.obstacles.Obstacles;
import gameGUI.GUI;
import utils.ImageDownloader;


import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


public class GameInterface extends JPanel implements KeyListener, Runnable {

    public static int WIDTH;
    public static int HEIGHT;
    private Thread animator;
    private boolean runGame = false;
    private boolean gameOver = false;
    private Landscape landscape;
    private Dinosaur dinosaur;
    private Obstacles obstacles;
    private int score;
    private int record;
    private int level;
    private ArrayList<BufferedImage> imagesList;
    private int speed;
    private BufferedImage sun;

    public GameInterface() {
        WIDTH = GUI.WIDTH;
        HEIGHT = GUI.HEIGHT;
        landscape = new Landscape(HEIGHT);
        dinosaur = new Dinosaur();
        obstacles = new Obstacles((int) (WIDTH * 1.5));
        score = 0;
        record = 0;
        level = 1;
        speed = 0;
        setSize(WIDTH, HEIGHT);
        setVisible(true);

    }

    public void paint(Graphics graphics) {
        super.paint(graphics);

        graphics.setFont(new Font("Arial", Font.BOLD, 25));
        graphics.drawString("SCORE", 800, 100);
        graphics.drawString(Integer.toString(score), 930, 100);
        graphics.drawString("RECORD", 800, 50);
        graphics.drawString(Integer.toString(record), 930, 50);
        graphics.drawString("LEVEL", 60, 50);
        graphics.drawString(Integer.toString(level), 160, 50);
        graphics.setFont(new Font("Arial", Font.BOLD, 10));
        graphics.drawString("designed by Grzybson", WIDTH - 120, HEIGHT - 50);
        sun = new ImageDownloader().getImageFromSource("../images/Sun.png");
        graphics.drawImage(sun, 750, 150, null);

        landscape.createLandscape(graphics);
        dinosaur.createDinosaur(graphics);
        obstacles.createObstacle(graphics);

        if (!runGame) {
            graphics.setFont(new Font("Bauhaus 93", Font.BOLD, 30));
            graphics.drawString("ARROW_UP: JUMP   ARROW_DOWN: LEAN", 200, 420);
            graphics.drawString("TAP SPACE_BAR TO START GAME", 250, 50);
        }
    }

    public void run() {
        runGame = true;
        setBackground(Color.WHITE);

        while (runGame) {
            updateGame();
            repaint();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateGame() {
        score += 1;
        if (score > 200 && score <= 500) {
            obstacles.increaseSpeed1Level();
            setBackground(Color.gray);
            level = 2;

        } else if (score > 500 && score <= 1000) {
            obstacles.increaseSpeed2Level();
            setBackground(Color.cyan);
            level = 3;

        } else if (score > 1000 && score <= 1700) {
            obstacles.increaseSpeed3Level();
            setBackground(Color.green);
            level = 4;

        } else if (score > 1700 && score <= 3000) {
            obstacles.increaseSpeed4Level();
            setBackground(Color.yellow);
            level = 5;

        } else if (score > 3000 && score <= 5000) {
            obstacles.increaseSpeed5Level();
            setBackground(Color.orange);
            level = 6;

        } else if (score > 5000 && score <= 10000) {
            obstacles.increaseSpeed6Level();
            setBackground(Color.red);
            level = 7;

        } else if (score > 10000) {
            obstacles.deadlyIncreaseSpeed();
            setBackground(Color.black);
            level = 6;
        }

        landscape.updateLandscape();
        obstacles.update();

        if (obstacles.obstacleCollision()) {
            int scoreSum = score;
            if (scoreSum > record) {
                record = scoreSum;
            }

            dinosaur.injured();
            repaint();
            runGame = false;
            gameOver = true;
            System.out.println("T-rex collision  GAME OVER");

        }

    }

    public void reset() {
        score = 0;
        level = 1;
        System.out.println("reset game");
        obstacles.restart();
        gameOver = false;
        setBackground(Color.WHITE);
    }

    public void keyTyped(KeyEvent event) {

        if (event.getKeyChar() == ' ') {
            if (gameOver) {
                reset();
            }
            if (animator == null || !runGame) {
                System.out.println("start the game");
                animator = new Thread(this);
                animator.start();
                dinosaur.startRunning();
            }
        }
    }

    public void keyPressed(KeyEvent event) {

        int key = event.getKeyCode();

        if ((key == KeyEvent.VK_DOWN)) {
            dinosaur.lean();
        } else if (key == KeyEvent.VK_UP) {
            dinosaur.jump();
        }
    }

    public void keyReleased(KeyEvent event) {
    }

}
