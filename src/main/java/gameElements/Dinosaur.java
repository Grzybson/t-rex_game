package gameElements;

import utils.ImageDownloader;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Dinosaur {

    private static final int STANDING = 1, RUNNING = 2, JUMPING = 3, LEANING = 4, INJURED = 5;
    private final int LEFT_FOOT = 1, RIGHT_FOOD = 2, NO_FOOT = 3;
    private int foot;
    private static int dinoStartX, dinoTopY, dinoTop, dinoLeaningTop, topPoint;
    private static boolean topPointReached;
    private static int jumpValue = 20;
    private static int dinosaurState;


    private static BufferedImage dinoStand;
    private BufferedImage leftFootDino;
    private BufferedImage rightFootDino;
    private BufferedImage leaningLeftFootDino;
    private BufferedImage leaningRightFootDino;
    private BufferedImage injuredDino;
    private BufferedImage gameOver;

    public Dinosaur() {

        leftFootDino = new ImageDownloader().getImageFromSource("../images/Dino_left_up.png");
        rightFootDino = new ImageDownloader().getImageFromSource("../images/Dino_right_up.png");
        dinoStand = new ImageDownloader().getImageFromSource("../images/Dino_stand.png");
        leaningLeftFootDino = new ImageDownloader().getImageFromSource("../images/Dino_leaning_left_foot.png");
        leaningRightFootDino = new ImageDownloader().getImageFromSource("../images/Dino_leaning_right_foot.png");
        injuredDino = new ImageDownloader().getImageFromSource("../images/Dino_big_eyes.png");
        gameOver = new ImageDownloader().getImageFromSource("../images/GAMEOVER.png");

        foot = NO_FOOT;
        dinosaurState = 1;
        dinoTopY = Landscape.LANDSCAPE_Y - dinoStand.getHeight() - 90;
        dinoLeaningTop = Landscape.LANDSCAPE_Y - dinoStand.getHeight() + 24;
        dinoStartX = 100;
        topPoint = dinoTopY - 120;

    }

    public void createDinosaur(Graphics graphics) {

        switch (dinosaurState) {
            case STANDING:
                graphics.drawImage(dinoStand, dinoStartX, dinoTopY, null);
                break;

            case RUNNING:
                if (foot == NO_FOOT) {
                    foot = LEFT_FOOT;
                    graphics.drawImage(leftFootDino, dinoStartX, dinoTopY, null);
                } else if (foot == LEFT_FOOT) {
                    foot = RIGHT_FOOD;
                    graphics.drawImage(rightFootDino, dinoStartX, dinoTopY, null);
                } else {
                    foot = LEFT_FOOT;
                    graphics.drawImage(leftFootDino, dinoStartX, dinoTopY, null);
                }
                break;

            case JUMPING:
                if (dinoTop > topPoint && !topPointReached) {
                    graphics.drawImage(dinoStand, dinoStartX, dinoTop -= jumpValue, null);
                    break;
                }
                if (dinoTop >= topPoint && !topPointReached) {
                    topPointReached = true;
                    graphics.drawImage(dinoStand, dinoStartX, dinoTop += jumpValue, null);
                    break;
                }
                if (dinoTop > topPoint && topPointReached) {
                    if (dinoTopY == dinoTop && topPointReached) {
                        dinosaurState = RUNNING;
                        topPointReached = false;
                        break;
                    }
                    graphics.drawImage(dinoStand, dinoStartX, dinoTop += jumpValue, null);
                    break;
                }

            case LEANING:
                if (foot == NO_FOOT) {
                    foot = LEFT_FOOT;
                    graphics.drawImage(leaningLeftFootDino, dinoStartX, dinoLeaningTop, null);
                } else if (foot == LEFT_FOOT) {
                    foot = RIGHT_FOOD;
                    graphics.drawImage(leaningRightFootDino, dinoStartX, dinoLeaningTop, null);
                } else {
                    foot = LEFT_FOOT;
                    graphics.drawImage(leaningLeftFootDino, dinoStartX, dinoLeaningTop, null);
                }
                break;

            case INJURED:
                graphics.drawImage(injuredDino, dinoStartX, dinoTop, null);
                graphics.drawImage(gameOver, 430, Landscape.LANDSCAPE_Y - gameOver.getHeight() - 100, null);
                break;
        }
    }

    public static Rectangle getDinosaur() {
        Rectangle dinosaur = new Rectangle();
        dinosaur.x = dinoStartX;

        if (dinosaurState == JUMPING && !topPointReached) {
            dinosaur.y = dinoTop - jumpValue;
        } else if (dinosaurState == JUMPING && topPointReached) {
            dinosaur.y = dinoTop + jumpValue;
        } else if (dinosaurState != JUMPING) {
            dinosaur.y = dinoTop;
        }
        dinosaur.width = dinoStand.getWidth();
        dinosaur.height = dinoStand.getHeight();

        return dinosaur;
    }

    public void startRunning() {
        dinoTop = dinoTopY;
        dinosaurState = RUNNING;
    }

    public void jump() {
        dinoTop = dinoTopY;
        topPointReached = false;
        dinosaurState = JUMPING;
    }

    public void lean() {
        dinoTop = dinoLeaningTop;
        dinosaurState = LEANING;
    }

    public void injured() {
        dinosaurState = INJURED;
    }

}




