package gameElements.obstacles;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Obstacle {

    BufferedImage obstacleImage;
    int width;
    int height;

    Rectangle getObstacle() {
        Rectangle obstacle = new Rectangle();
        obstacle.x = width;
        obstacle.y = height;
        obstacle.width = obstacleImage.getWidth();
        obstacle.height = obstacleImage.getHeight();

        return obstacle;
    }
}
