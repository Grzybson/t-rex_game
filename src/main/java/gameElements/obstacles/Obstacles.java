package gameElements.obstacles;

import gameElements.Dinosaur;
import gameElements.Landscape;
import utils.ImageDownloader;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


public class Obstacles {

    private int widthSize, heightSize;
    private int firstX;
    private int obstacleInterval;
    private int obstacleSpeed;
    private ArrayList<BufferedImage> imagesList;
    private ArrayList<Obstacle> obstaclesList;
    BufferedImage gameOver;


    public Obstacles(int firstPosition) {

        obstaclesList = new ArrayList<>();
        imagesList = new ArrayList<>();
        gameOver = new ImageDownloader().getImageFromSource("../images/GAMEOVER.png");
        firstX = firstPosition;
        obstacleInterval = setObstacleInterval();
        obstacleSpeed = 15;
        widthSize = 300;
        heightSize = Landscape.LANDSCAPE_Y - gameOver.getHeight() - 50;


        imagesList.add(new ImageDownloader().getImageFromSource("../images/Cactus1.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Cactus2.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Cactus2.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Cactus1.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Ceiling.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Cactus3.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Cactus4.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Cactus5.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Cactus3.png"));
        imagesList.add(new ImageDownloader().getImageFromSource("../images/Wall.png"));

        setObstaclesOnLandscape();
    }

    public void restart() {
        setObstaclesOnLandscape();
    }

    public void update() {
        Iterator<Obstacle> i = obstaclesList.iterator();
        Obstacle firstObstacle = i.next();
        firstObstacle.width = firstObstacle.width - obstacleSpeed;

        while (i.hasNext()) {
            Obstacle obstacle = i.next();
            obstacle.width = obstacle.width - obstacleSpeed;
        }
        Obstacle lastObstacle = obstaclesList.get(obstaclesList.size() - 1);
        if (firstObstacle.width < -firstObstacle.obstacleImage.getWidth()) {
            obstaclesList.remove(firstObstacle);
            firstObstacle.width = obstaclesList.get(obstaclesList.size() - 1).width + obstacleInterval;
            obstaclesList.add(firstObstacle);
        }

    }

    public void createObstacle(Graphics graphics) {
        for (Obstacle obstacle : obstaclesList) {
            graphics.setColor(Color.BLACK);
            graphics.drawImage(obstacle.obstacleImage, obstacle.width, obstacle.height, null);

        }
    }

    public boolean obstacleCollision() {
        for (Obstacle o : obstaclesList) {
            if (Dinosaur.getDinosaur().intersects(o.getObstacle())) {
                System.out.println("Dinosaur is injured");
                return true;
            }
        }
        return false;
    }

    public void setObstaclesOnLandscape() {

        int countObstacle = 0;
        int x = firstX;
        obstaclesList = new ArrayList<>();

        for (BufferedImage b : imagesList) {
            Obstacle obstacle = new Obstacle();
            obstacle.obstacleImage = b;
            obstacle.width = x;
            if (countObstacle == 4) {
                obstacle.height = Landscape.LANDSCAPE_Y - b.getHeight() - 22;
            } else if(countObstacle == 9) {
                obstacle.height = Landscape.LANDSCAPE_Y - b.getHeight() + 5;
            }else {
                obstacle.height = Landscape.LANDSCAPE_Y - b.getHeight() + 10;
            }

            obstacleInterval = setObstacleInterval();
            x = x + obstacleInterval;
            obstaclesList.add(obstacle);
            countObstacle++;
        }


    }

    public int setObstacleInterval() {
        Random r = new Random();
        int minInterval = 200;
        int maxInterval = 600;
        obstacleInterval = r.nextInt(maxInterval - minInterval + 1) + minInterval;
        return obstacleInterval;

    }

    public void increaseSpeed1Level() {
        obstacleSpeed = 16;
    }

    public void increaseSpeed2Level() {
        obstacleSpeed = 18;
    }

    public void increaseSpeed3Level() {
        obstacleSpeed = 21;
    }

    public void increaseSpeed4Level() {
        obstacleSpeed = 24;
    }

    public void increaseSpeed5Level() {
        obstacleSpeed = 27;
    }

    public void increaseSpeed6Level() {
        obstacleSpeed = 30;
    }

    public void deadlyIncreaseSpeed() {
        obstacleSpeed = 35;
    }


}


