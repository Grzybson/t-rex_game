package gameElements;

import utils.ImageDownloader;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;

public class Landscape {

    private class LandscapeImage {
        BufferedImage image;
        int x;
    }

    public static int LANDSCAPE_Y;
    private BufferedImage landscape;
    private ArrayList<LandscapeImage> landscapeImagesList;


    public Landscape(int boardHeight) {
        LANDSCAPE_Y = (int) (boardHeight - 0.3 * boardHeight);
        try {
            landscape = new ImageDownloader().getImageFromSource("../images/Landscape.png");
        } catch (Exception e) {
            e.printStackTrace();
        }
        landscapeImagesList = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            LandscapeImage landscapeImage = new LandscapeImage();
            landscapeImage.image = landscape;
            landscapeImage.x = 0;
            landscapeImagesList.add(landscapeImage);
        }
    }

    public void updateLandscape() {
        Iterator<LandscapeImage> loop = landscapeImagesList.iterator();
        LandscapeImage first = loop.next();
        first.x -= 10;
        int previousX = first.x;

        while (loop.hasNext()) {
            LandscapeImage next = loop.next();
            next.x = previousX + landscape.getWidth();
            previousX = next.x;
        }
        if (first.x < -landscape.getWidth()) {
            landscapeImagesList.remove(first);
            first.x = previousX + landscape.getWidth();
            landscapeImagesList.add(first);
        }
    }

    public void createLandscape(Graphics graphics) {
        for (LandscapeImage e : landscapeImagesList) {
            graphics.drawImage(e.image, e.x, LANDSCAPE_Y, null);
        }
    }

}
