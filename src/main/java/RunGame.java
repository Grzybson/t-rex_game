import gameGUI.GUI;

public class RunGame {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GUI().createAndShowGUI();
            }
        });
    }
}
