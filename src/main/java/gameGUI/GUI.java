package gameGUI;

import gamePanel.GameInterface;

import javax.swing.*;
import java.awt.*;

public class GUI {

    JFrame gameWindow = new JFrame("T-Rex run - Game");

    public static final int WIDTH = 1000;
    public static final int HEIGHT = 500;

    public void createAndShowGUI() {
        gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = gameWindow.getContentPane();
        GameInterface gameInterface = new GameInterface();
        gameInterface.addKeyListener(gameInterface);
        gameInterface.setFocusable(true);

        container.setLayout(new BorderLayout());
        container.add(gameInterface, BorderLayout.CENTER);
        gameWindow.setSize(WIDTH, HEIGHT);
        gameWindow.setResizable(false);
        gameWindow.setVisible(true);

    }

}
