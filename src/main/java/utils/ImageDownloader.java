package utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ImageDownloader {

    public BufferedImage getImageFromSource(String imagePath){
        BufferedImage bufferedImage = null;
        try{
            bufferedImage = ImageIO.read(getClass().getResource(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bufferedImage;
    }


}
